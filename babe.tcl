# Reproduction of babe-qt in Tk

package require Ttk
wm minsize . 200 200

# Make custom button
proc mybutton { b txt cmd } {
    label $b -text $txt
    bind $b <Enter> [list $b configure -fg fuchsia]
    bind $b <Leave> [list $b configure -fg black]
    bind $b <Button-1> $cmd 
    return $b
}

# Show a variant of the display area
proc showframe { f } {
    foreach c [ pack slaves .m ] {
        puts $c
        pack forget $c 
    }
    pack $f -in .m -fill both -expand yes
}


# ---
# Top window has main area and status bar
# ---
pack [ frame .m ] -side top -fill both -expand yes
pack [ frame .s ] -side bottom -fill x -expand no 

# ---
# Variants of display area: block, vertical frame and full-size fram
# ---

# block sized frame
set cover [ image create photo -file "babe.png" ]
label .b -bg AliceBlue -height 200 -width 200 -image $cover
place [ frame .b.float ] -anchor n -relx 0.5 -y 10
mybutton .b.float.like "Like" {puts "Like"}
mybutton .b.float.stop "Stop" {puts "Stop"}
pack .b.float.like .b.float.stop -side left -padx 2 -pady 2


# vertical frame
frame .v -bg BlueViolet -height 400 -width 200
pack [ frame .l -bg DeepSkyBlue -height 200 -width 200 ] \
    -in .v -side bottom -fill x -expand yes


# full sized frame
frame .f -bg CadetBlue -height 400 -width 600


# Populate status bar
mybutton .s.b "Block" {pack forget .b ; showframe .b}

mybutton .s.v "Strip" {
    pack forget .b
    pack .b -in .v -side top -fill both -expand yes
    showframe .v
}

mybutton .s.f "Full" {showframe .f} 

pack .s.b .s.v .s.f -side left -padx 2 -pady 2
